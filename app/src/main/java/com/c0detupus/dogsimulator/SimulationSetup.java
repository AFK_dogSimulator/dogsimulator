package com.c0detupus.dogsimulator;

import android.app.AlertDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.TimePicker;

import com.c0detupus.dogsimulator.enums.BroadcastEnums;
import com.c0detupus.dogsimulator.helper.DogDBAdapter;
import com.c0detupus.dogsimulator.enums.ToastMsgSimSet;
import com.c0detupus.dogsimulator.helper.Helper;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


public class SimulationSetup extends ActionBarActivity {


    private EditText inputDogNameSimSet;
    private TextView displayHoursSimSet;
    private TextView displayDaysSimSet;
    private SeekBar seekBarSimSet;

    private final int MAXIMUM_CHARS_ALLOWED = 12;
    private final int STANDARD_DAY_AMOUNT = 7;
    private int hour = 7;
    private int min = 0;


    private String dogName;
    private int dayAmount;

    private boolean nameTaken = true;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();

        setContentView(R.layout.activity_simulation_setup);

        inputDogNameSimSet = (EditText) findViewById(R.id.inputDogNameSimSet);
        displayHoursSimSet = (TextView) findViewById(R.id.displayHoursSimSet);
        displayDaysSimSet = (TextView) findViewById(R.id.displayDaysSimSet);
        seekBarSimSet = (SeekBar) findViewById(R.id.seekBarSimSet);


        inputDogNameSimSet.setFilters(new InputFilter[]{new InputFilter.LengthFilter(MAXIMUM_CHARS_ALLOWED)});

        inputDogNameSimSet.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (count >= MAXIMUM_CHARS_ALLOWED) {
                    displayToast(ToastMsgSimSet.MAXLETTERS);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {

            }

        });

        inputDogNameSimSet.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {

                    dogName = inputDogNameSimSet.getText().toString();
                    new NameDuplicateController().execute(dogName);

                    InputMethodManager inputManager = (InputMethodManager)
                            getSystemService(SimulationSetup.this.INPUT_METHOD_SERVICE);
                    inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                            InputMethodManager.HIDE_NOT_ALWAYS);

                    return true;
                }
                return false;
            }

        });


        seekBarSimSet.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                displayDaysSimSet.setText("" + progress);

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        seekBarSimSet.requestFocus();

        setUpAndDisplayStandardValues();


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_simulation_setup, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.to_score_screen) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void setTime(View view) {


        new TimePickerDialog(this,
                picker, hour, min, true).show();


    }


    public void startSimulation(final View view) {

        boolean approved = true;

        dogName = inputDogNameSimSet.getText().toString();
        dayAmount = seekBarSimSet.getProgress();

        if (!onlyLetters(dogName)) {
            displayToast(ToastMsgSimSet.ONLYATOZ);
            approved = false;
        }

        if (dayAmount == 0) {
            displayToast(ToastMsgSimSet.ZERODAYS);
            approved = false;
        }

        if (nameTaken) {

            displayToast(ToastMsgSimSet.NAMEINDATABASE);
            approved = false;
        }


        if (approved) {
            new AlertDialog.Builder(this)

                    .setTitle("Are you sure you want to start the simulation?")
                    .setMessage("Dog name:" + dogName + "\nDays of simulation: " +
                            dayAmount + "\nDog wakes up at: "
                            + displayHoursSimSet.getText().toString())

                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                            new SaveChoices().execute();
                        }
                    })
                    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                            dialog.dismiss();


                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();

        }


    }


    private boolean onlyLetters(String name) {
        char[] chars = name.toCharArray();


        if (chars.length == 0) {
            return false;
        }

        for (char c : chars) {
            if (!Character.isLetter(c)) {
                return false;
            }
        }

        return true;
    }


    private void setUpAndDisplayStandardValues() {


        timeFormatter(hour, min);

        seekBarSimSet.setProgress(STANDARD_DAY_AMOUNT);


    }

    private void displayToast(ToastMsgSimSet delimeters) {

        String toastMsg = null;


        switch (delimeters) {
            case MAXLETTERS:
                toastMsg = "Maximum amount of letters is " + MAXIMUM_CHARS_ALLOWED;
                break;
            case ONLYATOZ:
                toastMsg = "ONLY letters A-Z are allowed in the name!";
                break;
            case ZERODAYS:
                toastMsg = "You cant have 0 days of simulation time";
                break;
            case NAMEINDATABASE:


                toastMsg = "You already had a dog named: " + dogName;

                break;
            default:
                break;
        }

        Helper.toaster(toastMsg, this);

    }


    private TimePickerDialog.OnTimeSetListener picker = new TimePickerDialog.OnTimeSetListener() {
        public void onTimeSet(TimePicker view, int selectedHour,
                              int selectedMinute) {
            timeFormatter(selectedHour, selectedMinute);

        }
    };


    private void timeFormatter(int selectedHour, int selectedMinute) {
        hour = selectedHour;
        min = selectedMinute;

        String formattedHour = null;
        String formattedMinute = null;

        StringBuilder sb = new StringBuilder();


        if (Integer.toString(hour).length() == 1) {

            sb.append("0");
            sb.append(hour);
            formattedHour = sb.toString();
            sb.setLength(0);
        } else {

            formattedHour = Integer.toString(hour);

        }

        if (Integer.toString(min).length() == 1) {

            sb.append("0");
            sb.append(min);
            formattedMinute = sb.toString();
            sb.setLength(0);

        } else {

            formattedMinute = Integer.toString(min);

        }

        sb.append(formattedHour);
        sb.append(":");
        sb.append(formattedMinute);

        displayHoursSimSet.setText(sb.toString());
    }


    private class NameDuplicateController extends AsyncTask<String, Void, Boolean> {


        @Override
        protected Boolean doInBackground(String... params) {

            DogDBAdapter dbAdapter = new DogDBAdapter(SimulationSetup.this);

            String name = params[0];

            if (dbAdapter.duplicateName(name)) {

                return true;
            }
            return false;
        }

        @Override
        protected void onPostExecute(Boolean nameExists) {

            if (nameExists) {
                nameTaken = true;

                displayToast(ToastMsgSimSet.NAMEINDATABASE);
            } else {

                nameTaken = false;
            }


        }


    }

    private class SaveChoices extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            Helper.toaster("Saving your choices please wait", SimulationSetup.this);
        }

        @Override
        protected Boolean doInBackground(Void... params) {

            DogDBAdapter dbAdapter = new DogDBAdapter(SimulationSetup.this);

            SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
            Calendar startDate = Calendar.getInstance();
            startDate.setTime(new Date());

            Calendar endDate = Calendar.getInstance();
            endDate.setTime(new Date());
            endDate.add(Calendar.DATE, dayAmount);

            String formatedStartDate = df.format(startDate.getTime());
            String formatedEndDate = df.format(endDate.getTime());



            long id = dbAdapter.insertOrUpdateDog(true, formatedStartDate, formatedEndDate,
                    dogName, 1, displayHoursSimSet.getText().toString(), 50, 50, 25);


            return id > 0;
        }


        @Override
        protected void onPostExecute(Boolean aBoolean) {
            if (aBoolean) {
                startActivity(new Intent(SimulationSetup.this, DogActivity.class));
                Intent intent = new Intent(BroadcastEnums.FINISHSETUP.toString());
                LocalBroadcastManager.getInstance(SimulationSetup.this).sendBroadcast(intent);
                SimulationSetup.this.finish();
            } else {
                Helper.toaster("Error when inserting data to DATABASE", SimulationSetup.this);
            }
        }
    }

}
