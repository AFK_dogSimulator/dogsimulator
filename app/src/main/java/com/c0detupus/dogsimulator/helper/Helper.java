package com.c0detupus.dogsimulator.helper;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.view.Gravity;
import android.widget.Toast;

import com.c0detupus.dogsimulator.DogActivity;
import com.c0detupus.dogsimulator.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by c0detupus on 2015-04-14.
 */
public class Helper {


    private static Toast toast;


    public static void toaster(String toastMsg, Context context) {

        if (toast != null) {
            toast.cancel();
        }

        int time = Toast.LENGTH_SHORT;
        CharSequence msg = toastMsg;
        toast = Toast.makeText(context, msg, time);
        toast.setGravity(Gravity.CENTER, 0, 0);

        toast.show();

    }


    public static Date timeParser(String stringTime) {

        SimpleDateFormat df = new SimpleDateFormat("HH:mm");
        Date d = null;

        try {
            d = df.parse(stringTime);
        } catch (ParseException e) {
            System.out.println("Problems in timeParser " + e);
        }

        return d;

    }


    public static void walkNotifier(Context context, boolean notify) {

        Intent resultIntent = new Intent(context, DogActivity.class);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        stackBuilder.addNextIntent(resultIntent);

        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);

        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        builder.setContentIntent(resultPendingIntent)
                .setSmallIcon(R.drawable.notification_icon_leash)
                .setContentTitle("Time for a walk!")
                .setContentText("Woof woof")
                .setAutoCancel(true)
                .setSound(alarmSound);

        NotificationManager mNotificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        if(notify){
            mNotificationManager.notify(1, builder.build());

        }else{
            mNotificationManager.cancel(1);
        }



    }



}
