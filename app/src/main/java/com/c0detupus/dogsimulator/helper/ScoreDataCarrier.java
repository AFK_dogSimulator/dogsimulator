package com.c0detupus.dogsimulator.helper;

/**
 * Created by c0detupus on 2015-04-20.
 */
public class ScoreDataCarrier {


    private static int avgKm = 0;
    private static int avgHappiness = 0;
    private static int dogAmount = 0;
    private static int totalKm = 0;

    public static int getAvgKm() {
        return avgKm;
    }

    public static void setAvgKm(int avgKm) {
        ScoreDataCarrier.avgKm = avgKm;
    }

    public static int getAvgHappiness() {
        return avgHappiness;
    }

    public static void setAvgHappiness(int avgHappiness) {
        ScoreDataCarrier.avgHappiness = avgHappiness;
    }

    public static int getDogAmount() {
        return dogAmount;
    }

    public static void setDogAmount(int dogAmount) {
        ScoreDataCarrier.dogAmount = dogAmount;
    }

    public static int getTotalKm() {
        return totalKm;
    }

    public static void setTotalKm(int totalKm) {
        ScoreDataCarrier.totalKm = totalKm;
    }
}
