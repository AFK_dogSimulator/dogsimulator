package com.c0detupus.dogsimulator.helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

import com.c0detupus.dogsimulator.model.Dog;

import java.sql.SQLOutput;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by c0detupus on 2015-04-08.
 */
public class DogDBAdapter {


    private DogDBHelper helper;
    private Context context;

    public DogDBAdapter(Context context) {

        this.context = context;
        helper = new DogDBHelper(context);

    }


    public long insertOrUpdateDog(boolean insert,
                                  String startDate,
                                  String endDate,
                                  String name,
                                  int active,
                                  String wakeUp,

                                  int happiness,
                                  int activity,
                                  int toilet) {

        SQLiteDatabase db = helper.getWritableDatabase();
        ContentValues values = new ContentValues();


        values.put(helper.DOG_HAPPINESS, happiness);
        values.put(helper.DOG_ACTIVITY, activity);
        values.put(helper.DOG_TOILET, toilet);

        long id = 0;

        if (insert) {

            values.put(helper.DOG_START_DATE, startDate);
            values.put(helper.DOG_END_DATE, endDate);
            values.put(helper.DOG_NAME, name);
            values.put(helper.DOG_ACTIVE, active);
            values.put(helper.DOG_WAKEUP, wakeUp);

            id = db.insert(helper.DOG_TABLE_NAME, null, values);

        } else {
            String where = helper.DOG_ACTIVE + " = ?";
            String[] args = {"1"};

            id = db.update(helper.DOG_TABLE_NAME, values, where, args);
        }

        db.close();
        return id;


    }


    public boolean duplicateName(String name) {


        SQLiteDatabase db = helper.getWritableDatabase();


        String[] columns = {helper.DOG_NAME};
        String where = helper.DOG_NAME + " = ?";
        String[] args = {name};
        Cursor cursor = db.query(helper.DOG_TABLE_NAME, columns, where, args, null, null, null);


        if (cursor != null && cursor.moveToFirst()) {

            if (name.equals(cursor.getString(cursor.getColumnIndex(helper.DOG_NAME)))) {
                cursor.close();
                db.close();
                return true;
            }


        }

        cursor.close();
        db.close();
        return false;
    }


    public Dog retrieveActiveDog() {


        String where = helper.DOG_ACTIVE + " = ?";
        String[] args = {"1"};


        SQLiteDatabase db = helper.getWritableDatabase();


        Cursor cursor = db.query(helper.DOG_TABLE_NAME, null, where, args, null, null, null);


        if (cursor != null && cursor.moveToFirst()) {


            Dog dog = new Dog();

            dog.setStartDate(cursor.getString(cursor.getColumnIndex(helper.DOG_START_DATE)));
            dog.setEndDate(cursor.getString(cursor.getColumnIndex(helper.DOG_END_DATE)));
            dog.set_id(cursor.getInt(cursor.getColumnIndex(helper.DOG_PRIMARY_KEY)));
            dog.setName(cursor.getString(cursor.getColumnIndex(helper.DOG_NAME)));
            dog.setActive(true);
            dog.setHappiness(cursor.getInt(cursor.getColumnIndex(helper.DOG_HAPPINESS)));
            dog.setActivity(cursor.getInt(cursor.getColumnIndex(helper.DOG_ACTIVITY)));
            dog.setWakeUp(cursor.getString(cursor.getColumnIndex(helper.DOG_WAKEUP)));
            dog.setToilet(cursor.getInt(cursor.getColumnIndex(helper.DOG_TOILET)));

            cursor.close();
            db.close();

            return dog;
        }


        return null;


    }


    public long endCurrentSimulation() {

        SQLiteDatabase db = helper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(helper.DOG_ACTIVE, 0);
        long id = db.update(helper.DOG_TABLE_NAME, values, null, null);
        db.close();

        return id;
    }


    public long insertOrUpdateLogData(String date, int happiness, int activity, int dailyKm) {

        Dog d = retrieveActiveDog();
        long dog_id = d.get_id();
        long id = 0;

        String where = helper.LOG_CURRENT_DATE + " = ?";
        String[] args = {date};

        SQLiteDatabase db = helper.getWritableDatabase();
        Cursor cursor = db.query(helper.LOG_TABLE_NAME, null, where, args, null, null, null);


        ContentValues values = new ContentValues();
        values.put(helper.LOG_HAPPINESS, happiness);
        values.put(helper.LOG_ACTIVITY, activity);


        //Update current database data
        if (cursor != null && cursor.moveToFirst()) {

            int accumulatedKm = cursor.getInt(cursor.getColumnIndex(helper.LOG_DAILY_KM));


            values.put(helper.LOG_DAILY_KM, accumulatedKm + dailyKm);

            cursor.close();

            id = db.update(helper.LOG_TABLE_NAME, values, where, args);

            db.close();


            //Insert new row with current date
        } else {

            values.put(helper.LOG_CURRENT_DATE, date);
            values.put(helper.LOG_DOG_ID, dog_id);
            values.put(helper.LOG_DAILY_KM, dailyKm);

            id = db.insert(helper.LOG_TABLE_NAME, null, values);

        }

        return id;
    }


    public void genericStats() {


        SQLiteDatabase db = helper.getWritableDatabase();

        Cursor a = db.rawQuery("SELECT avg(" + helper.LOG_DAILY_KM + "), " +
                "avg(" + helper.LOG_HAPPINESS + ") FROM "
                + helper.LOG_TABLE_NAME
                + ";", null);

        if (a != null && a.moveToNext()) {

            a.moveToFirst();

            ScoreDataCarrier.setAvgKm(a.getInt(0));
            ScoreDataCarrier.setAvgHappiness(a.getInt(1));

        }

        a.close();
        a = db.rawQuery("SELECT sum("+helper.LOG_DAILY_KM+") FROM "+helper.LOG_TABLE_NAME+";",null);



        if (a != null && a.moveToNext()) {

            a.moveToFirst();

            ScoreDataCarrier.setTotalKm(a.getInt(0));


        }

        a.close();

        a = db.rawQuery("SELECT count(*) FROM "+helper.DOG_TABLE_NAME+";", null);


        if (a != null && a.moveToNext()) {


            a.moveToFirst();


            ScoreDataCarrier.setDogAmount(a.getInt(0));


        }

        a.close();
        db.close();

    }


    private static class DogDBHelper extends SQLiteOpenHelper {

        private Context context;

        private static final String DATABASE_NAME = "dogsimdatabase";
        private static final int DATABASE_VERSION = 7;

        //DOG TABLE -->
        private static final String DOG_TABLE_NAME = "dogs";
        private static final String DOG_PRIMARY_KEY = "_id";
        private static final String DOG_START_DATE = "start_date";
        private static final String DOG_END_DATE = "end_date";
        private static final String DOG_NAME = "name";
        private static final String DOG_ACTIVE = "active";
        private static final String DOG_HAPPINESS = "happiness";
        private static final String DOG_ACTIVITY = "activity";
        private static final String DOG_WAKEUP = "wakeup";
        private static final String DOG_TOILET = "toilet";

        //CREATE TABLE QUERY
        private static final String CREATE_DOG_TABLE =
                "CREATE TABLE " + DOG_TABLE_NAME + "("
                        + DOG_PRIMARY_KEY + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                        + DOG_START_DATE + " VARCHAR(10), "
                        + DOG_END_DATE + " VARCHAR(10), "
                        + DOG_NAME + " VARCHAR(30) UNIQUE, "
                        + DOG_ACTIVE + " BOOLEAN NOT NULL CHECK (" + DOG_ACTIVE + " IN (0,1)), "
                        + DOG_HAPPINESS + " INTEGER, "
                        + DOG_ACTIVITY + " INTEGER, "
                        + DOG_WAKEUP + " VARCHAR(5), "
                        + DOG_TOILET + " INTEGER"
                        + ");";
        //<-- DOG TABLE
        //DAILY_LOG TABLE -->
        private static final String LOG_TABLE_NAME = "dailydoglogs";
        private static final String LOG_PRIMARY_KEY = "_id";
        private static final String LOG_DOG_ID = "dog_id";
        private static final String LOG_CURRENT_DATE = "current_date";
        private static final String LOG_HAPPINESS = "happiness";
        private static final String LOG_ACTIVITY = "activity";
        private static final String LOG_DAILY_KM = "daily_km";

        //CREATE TABLE QUERY
        private static final String CREATE_LOG_TABLE = "CREATE TABLE " + LOG_TABLE_NAME + "("
                + LOG_PRIMARY_KEY + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + LOG_DOG_ID + " INTEGER, "
                + LOG_CURRENT_DATE + " VARCHAR(10), "
                + LOG_HAPPINESS + " INTEGER, "
                + LOG_ACTIVITY + " INTEGER, "
                + LOG_DAILY_KM + " INTEGER, "
                + "FOREIGN KEY(" + LOG_DOG_ID + ") " +
                "REFERENCES " + DOG_TABLE_NAME + "(" + DOG_PRIMARY_KEY + "));";
        //<-- DAILY_LOG TABLE


        private static final String DROP_DOG_TABLE = "DROP TABLE IF EXISTS " + DOG_TABLE_NAME;
        private static final String DROP_LOG_TABLE = "DROP TABLE IF EXISTS " + LOG_TABLE_NAME;

        private DogDBHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
            this.context = context;
        }


        @Override
        public void onCreate(SQLiteDatabase db) {


            try {
                db.execSQL(CREATE_DOG_TABLE);
                db.execSQL(CREATE_LOG_TABLE);
                System.out.println("SUCCESSFULLY CREATED DATABASE");

            } catch (SQLiteException x) {
                System.out.println("UNSUCCESSFUL WHEN TRYING TO CREATE DATABASE " + x);
            }

        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {


            try {
                db.execSQL(DROP_DOG_TABLE);
                db.execSQL(DROP_LOG_TABLE);

                onCreate(db);
                System.out.println("SUCCESSFULLY UPGRADED DATABASE");

            } catch (SQLiteException x) {


                System.out.println("UNSUCCESSFUL WHEN TRYING TO UPGRADE DATABASE " + x);
            }

        }

    }


}
