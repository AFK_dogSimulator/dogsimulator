package com.c0detupus.dogsimulator;

import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.c0detupus.dogsimulator.helper.DogDBAdapter;
import com.c0detupus.dogsimulator.helper.ScoreDataCarrier;

public class ScoreScreen extends ActionBarActivity {

    private TextView
            displayAvgKmTV,
            displayAvgHappiness,
            displayDogAmount,
            displayTotalKm;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_score_screen);


        displayAvgKmTV = (TextView) findViewById(R.id.displayAvgKmTVStats);
        displayAvgHappiness = (TextView) findViewById(R.id.displayAvgHappinessTVStats);
        displayDogAmount = (TextView) findViewById(R.id.displayDogAmountTVStats);
        displayTotalKm = (TextView) findViewById(R.id.displayTotalKmStats);

        new DisplayStats().execute();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_score_screen, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.to_score_screen) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private class DisplayStats extends AsyncTask<Void, Void, Void>{


        @Override
        protected Void doInBackground(Void... params) {

            DogDBAdapter dbAdapter = new DogDBAdapter(ScoreScreen.this);
            dbAdapter.genericStats();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {

            displayAvgHappiness.setText(String.valueOf(ScoreDataCarrier.getAvgHappiness()));
            displayAvgKmTV.setText(String.valueOf(ScoreDataCarrier.getAvgKm()));
            displayTotalKm.setText(String.valueOf(ScoreDataCarrier.getTotalKm()));
            displayDogAmount.setText(String.valueOf(ScoreDataCarrier.getDogAmount()));

        }
    }

}
