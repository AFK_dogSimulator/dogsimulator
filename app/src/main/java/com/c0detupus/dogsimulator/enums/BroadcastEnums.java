package com.c0detupus.dogsimulator.enums;

/**
 * Created by c0detupus on 2015-04-16.
 */
public enum BroadcastEnums {


    TIMEFRAMESTART, TIMEFRAMEEND, FINISHSETUP;

    private BroadcastEnums(){

    }

}
