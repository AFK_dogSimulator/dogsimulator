package com.c0detupus.dogsimulator.enums;

/**
 * Created by c0detupus on 2015-04-07.
 */
public enum ToastMsgSimSet {

    MAXLETTERS, ONLYATOZ, ZERODAYS, NAMEINDATABASE;

    private ToastMsgSimSet(){

    }

}
