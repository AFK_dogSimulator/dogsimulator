package com.c0detupus.dogsimulator;

import android.app.ActionBar;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.c0detupus.dogsimulator.enums.BroadcastEnums;
import com.c0detupus.dogsimulator.helper.DogDBAdapter;
import com.c0detupus.dogsimulator.helper.Helper;
import com.c0detupus.dogsimulator.model.Dog;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


public class DogActivity extends ActionBarActivity implements LocationListener {


    private LocationManager lm;
    private Location last;
    private int distance;

    private AlarmManager alarmMgr;
    private PendingIntent alarmIntent;
    private String wakeUpTime;

    private TextView dogName, loadingDataTV, displayWakeUptimeTV, displayDaysLeftTV;
    private ProgressBar loadingDataProgressbar,
            happinessProgressbar,
            activityProgressbar,
            toiletProgresbar;

    private Button walkButton;
    private Button stopWalkButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_dog_main);

        dogName = (TextView) findViewById(R.id.dogNameMain);
        loadingDataTV = (TextView) findViewById(R.id.loadingDataTVMain);
        displayWakeUptimeTV = (TextView) findViewById(R.id.displayWakeupTVMain);
        displayDaysLeftTV = (TextView) findViewById(R.id.displayDaysLeftTVMain);

        loadingDataProgressbar = (ProgressBar) findViewById(R.id.loadingDataProgressbarMain);
        happinessProgressbar = (ProgressBar) findViewById(R.id.happinessProgressbarMain);
        activityProgressbar = (ProgressBar) findViewById(R.id.activityProgressbarMain);
        toiletProgresbar = (ProgressBar) findViewById(R.id.toiletProgressbarMain);

        walkButton = (Button) findViewById(R.id.walkButton);
        stopWalkButton = (Button) findViewById(R.id.stopWalkButton);


    }


    @Override
    protected void onStart() {
        super.onStart();
        new PopulateDogData().execute();
    }

    @Override
    public void onBackPressed() {

        this.finish();

    }

    @Override
    protected void onResume() {
        super.onResume();
        new PopulateDogData().execute();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_dog_main, menu);


        ActionBar actionBar = getActionBar();
        if (actionBar != null) {
            actionBar.setHomeButtonEnabled(false); // disable the button
            actionBar.setDisplayHomeAsUpEnabled(false); // remove the left caret
            actionBar.setDisplayShowHomeEnabled(false); // remove the icon
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.to_score_screen) {
            startActivity(new Intent(this, ScoreScreen.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    // PUBLIC METHODS -->
    public void goForAWalk(View view) {
        lm = (LocationManager) getSystemService(LOCATION_SERVICE);
        lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 5, this);

        walkButton.setVisibility(View.GONE);
        stopWalkButton.setVisibility(View.VISIBLE);
    }

    public void stopWalk(View view) {
        locationManagerStopper();

        new CalculateAndSave().execute();

        stopWalkButton.setVisibility(View.GONE);
        walkButton.setVisibility(View.VISIBLE);

    }

    public void endSimulation(View view) {

        new CalculateAndSave().execute();
        new EndSimulation().execute();

    }

    public void toScoreScreen(View view) {
        startActivity(new Intent(this, ScoreScreen.class));
    }
    // <-- PUBLIC METHODS


    // ALARMSETUP METHODS -->

    private boolean noActivePendingIntent() {


        return (PendingIntent.getBroadcast(this, 0,
                new Intent(this, ProgressUpdater.class),
                PendingIntent.FLAG_NO_CREATE) == null);


    }


    private void alarmIntentSetup() {

        Date timeFrameEnd = Helper.timeParser(wakeUpTime);
        Date timeFrameStart;

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(timeFrameEnd);

        timeFrameStart = calendar.getTime();
        calendar.add(Calendar.HOUR, 16);

        timeFrameEnd = calendar.getTime();

        SimpleDateFormat df = new SimpleDateFormat("HH:mm");


        Intent intent = new Intent(this, ProgressUpdater.class);
        intent.putExtra(BroadcastEnums.TIMEFRAMESTART.toString(), df.format(timeFrameStart));
        intent.putExtra(BroadcastEnums.TIMEFRAMEEND.toString(), df.format(timeFrameEnd));

        alarmIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    private void alarmManagerStart() {


        //         4 hours
        int cycle = 1000 * 60 * 60 * 4;


        Date d = Helper.timeParser(wakeUpTime);

        Calendar calendar = Calendar.getInstance();

        calendar.setTime(d);

        alarmIntentSetup();

        alarmMgr = (AlarmManager) getSystemService(ALARM_SERVICE);
        alarmMgr.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),
                cycle, alarmIntent);


    }

    private void alarmManagerStopper() {


        Intent intent = new Intent(this, ProgressUpdater.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);


        pendingIntent.cancel();

    }
    // <-- ALARMSETUP METHODS


    // LOCATION METHODS -->

    private void locationManagerStopper() {
        if (lm != null) {
            lm.removeUpdates(this);

        }
    }

    @Override
    public void onLocationChanged(Location location) {
        if (last != null) {
            distance += location.distanceTo(last);
        }

        last = new Location(location);

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
    // <-- LOCATION METHODS


    // ASYNCTASK CLASSES-->
    private class PopulateDogData extends AsyncTask<Void, Void, Dog> {


        @Override
        protected Dog doInBackground(Void... params) {

            DogDBAdapter dbAdapter = new DogDBAdapter(DogActivity.this);


            return dbAdapter.retrieveActiveDog();

        }


        @Override
        protected void onPostExecute(Dog dog) {

            if (dog != null) {

                loadingDataTV.setVisibility(View.GONE);
                loadingDataProgressbar.setVisibility(View.GONE);

                dogName.setVisibility(View.VISIBLE);

                dogName.setText(dog.getName());

                happinessProgressbar.setProgress(dog.getHappiness());
                activityProgressbar.setProgress(dog.getActivity());
                toiletProgresbar.setProgress(dog.getToilet());
                wakeUpTime = dog.getWakeUp();
                displayWakeUptimeTV.setText(wakeUpTime);

                String s = null;
                int dl = dog.getDaysLeft();

                switch (dl) {

                    case -1:
                        new EndSimulation().execute();
                        break;
                    case 0:
                        s = "Last day";
                        break;
                    default:
                        s = Integer.toString(dl);

                }


                displayDaysLeftTV.setText(s);

                if (noActivePendingIntent()) {
                    alarmManagerStart();
                }


            }


        }
    }

    private class EndSimulation extends AsyncTask<Void, Void, Boolean> {


        @Override
        protected Boolean doInBackground(Void... params) {


            DogDBAdapter dbAdapter = new DogDBAdapter(DogActivity.this);

            if (dbAdapter.endCurrentSimulation() > 0) {
                return true;
            }

            return (dbAdapter.endCurrentSimulation() > 0);
        }


        @Override
        protected void onPostExecute(Boolean simulationEnded) {

            if (simulationEnded) {


                alarmManagerStopper();

                Helper.walkNotifier(DogActivity.this, false);
                locationManagerStopper();
                startActivity(new Intent(DogActivity.this, StartUp.class));
                DogActivity.this.finish();
            }

        }

    }

    private class CalculateAndSave extends AsyncTask<Void, Void, Dog> {


        @Override
        protected Dog doInBackground(Void... params) {

            DogDBAdapter dbAdapter = new DogDBAdapter(DogActivity.this);

            SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
            String date = df.format(Calendar.getInstance().getTime());

            Dog dog = dbAdapter.retrieveActiveDog();

            dog.activityCalculator(distance);

            dbAdapter.insertOrUpdateLogData(date, dog.getHappiness(), dog.getActivity(), distance);

            dbAdapter.insertOrUpdateDog(false, null, null, null, 0, null, dog.getHappiness(),
                    dog.getActivity(), dog.getToilet());

            return dog;
        }

        @Override
        protected void onPostExecute(Dog dog) {


            happinessProgressbar.setProgress(dog.getHappiness());
            activityProgressbar.setProgress(dog.getActivity());
            toiletProgresbar.setProgress(dog.getToilet());


        }
    }
    // <-- ASYNCTASK CLASSES
}
