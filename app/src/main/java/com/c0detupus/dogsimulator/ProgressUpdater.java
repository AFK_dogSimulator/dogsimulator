package com.c0detupus.dogsimulator;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.c0detupus.dogsimulator.enums.BroadcastEnums;
import com.c0detupus.dogsimulator.helper.DogDBAdapter;
import com.c0detupus.dogsimulator.helper.Helper;
import com.c0detupus.dogsimulator.model.Dog;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by c0detupus on 2015-04-16.
 */
public class ProgressUpdater extends BroadcastReceiver {


    @Override
    public void onReceive(Context context, Intent intent) {


        System.out.println("recieved");


        String frameStart = intent.getStringExtra(BroadcastEnums.TIMEFRAMESTART.toString());
        String frameEnd = intent.getStringExtra(BroadcastEnums.TIMEFRAMEEND.toString());


        int hour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
        int min = Calendar.getInstance().get(Calendar.MINUTE);

        Date timeFrameStart = Helper.timeParser(frameStart);
        Date timeFrameEnd = Helper.timeParser(frameEnd);
        Date now = Helper.timeParser(hour + ":" + min);


        System.out.println("Now: " + now.getTime() + " Start: " + timeFrameStart.getTime() + " End: " + timeFrameEnd.getTime());


        if (now.after(timeFrameStart) && now.before(timeFrameEnd)) {


            regularUpdate(context);
            Helper.walkNotifier(context, true);
        }


    }


    private void regularUpdate(Context context) {

        System.out.println("UPDATE");
        DogDBAdapter dbAdapter = new DogDBAdapter(context);
        Dog dog = dbAdapter.retrieveActiveDog();

        dog.fourHourCycle();

        dbAdapter.insertOrUpdateDog(false, null, null, null, 0, null,
                dog.getHappiness(),
                dog.getActivity(),
                dog.getToilet()


        );


    }



}


