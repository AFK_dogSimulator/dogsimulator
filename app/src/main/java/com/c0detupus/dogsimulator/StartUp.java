package com.c0detupus.dogsimulator;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;

import com.c0detupus.dogsimulator.enums.BroadcastEnums;
import com.c0detupus.dogsimulator.helper.DogDBAdapter;


public class StartUp extends ActionBarActivity {



    private RelativeLayout startUpRelativeLayout;
    private RelativeLayout loadingDataRelativeLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_up);

        this.startUpRelativeLayout = (RelativeLayout) findViewById(R.id.startUpRelativeLayout);
        this.loadingDataRelativeLayout = (RelativeLayout) findViewById(R.id.loadingDataRelativeLayout);



        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver,
                new IntentFilter(BroadcastEnums.FINISHSETUP.toString()));


        new SimulationController().execute();


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_start_up, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.to_score_screen) {
            startActivity(new Intent(this, ScoreScreen.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        // Unregister since the activity is about to be closed.
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
        super.onDestroy();
    }


    public void toSimulationSetup(View view) {

        startActivity(new Intent(this, SimulationSetup.class));


    }


    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {



                StartUp.this.finish();


        }
    };


   private class SimulationController extends AsyncTask<Void, Void, Boolean> {



        @Override
        protected Boolean doInBackground(Void... params) {


          DogDBAdapter dbAdapter = new DogDBAdapter(StartUp.this);

            if (dbAdapter.retrieveActiveDog() != null) {


                return true;

            }

            return false;
        }

        @Override
        protected void onPostExecute(Boolean state) {

            if (state) {
                startActivity(new Intent(StartUp.this, DogActivity.class));
                StartUp.this.finish();
            } else {
                loadingDataRelativeLayout.setVisibility(View.GONE);
                startUpRelativeLayout.setVisibility(View.VISIBLE);
            }


        }


    }
}






