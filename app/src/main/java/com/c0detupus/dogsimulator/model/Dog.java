package com.c0detupus.dogsimulator.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by c0detupus on 2015-04-01.
 */
public class Dog {

    private final int SIMULATION_TIME_OVER_CODE = -1;

    private final int MAXIMUM_VALUE = 100;
    private final int MINIMUM_VALUE = 0;

    private String startDate;
    private String endDate;

    private long _id;
    private String name;

    private boolean active;


    private int happiness;

    private int activity;

    private String wakeUp;

    // high is bad
    // 0 on startup and when relieved
    // 25 + every hour
    // active between the 16 waking hours of the day
    private int toilet;
    // <-- to here


    private int dailyKm;

    public Dog() {


    }

    //SETTERS -->


    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public void set_id(long _id) {this._id = _id;}

    public void setName(String name) {
        this.name = name;
    }

    public void setActive(boolean active) {
        this.active = active;
    }


    public void setHappiness(int happiness) {
        this.happiness = happiness;
    }

    public void setActivity(int activity) {
        this.activity = activity;
    }

    public void setWakeUp(String wakeUp) {
        this.wakeUp = wakeUp;
    }

    public void setToilet(int natureCall) {
        this.toilet = natureCall;
    }
    //<-- SETTERS

    //GETTERS -->
    public boolean getActive() {
        return active;
    }


    public long get_id() { return _id; }

    public String getName() {
        return name;
    }


    //
    public int getDaysLeft() {

        if (startDate.equals(endDate)) {
            return SIMULATION_TIME_OVER_CODE;
        }

        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");

        Date start = Calendar.getInstance().getTime();
        Date end = null;
        try {

            end = df.parse(endDate);
        } catch (ParseException x) {
            System.out.println("Exception when trying to calculate days left " + x);
        }


        long diff = end.getTime() - start.getTime();

        int result = (int) (diff / (24 * 60 * 60 * 1000));


        return result;

    }


    public int getHappiness() {
        return happiness;
    }

    public int getActivity() {
        return activity;
    }

    public String getWakeUp() {
        return wakeUp;
    }

    public int getToilet() {
        return toilet;
    }
    //<--GETTERS


    //INCREMENTERS -->


    private void incrementHappiness(int happinessPoints) {
        happiness = upperValueLimiter(happiness, happinessPoints);
    }

    private void incrementActivity(int activityPoints) {
        activity = upperValueLimiter(activity, activityPoints);
    }

    private void incrementToilet(int toiletPoints) {
        toilet = upperValueLimiter(toilet, toiletPoints);
    }


    //<-- INCREMENTERS


    //DECREMENTERS -->


    private void decrementHappiness(int happinessPoints) {
        happiness = lowerValueLimiter(happiness, happinessPoints);
    }


    private void decrementActivity(int activityPoints) {
        activity = lowerValueLimiter(activity, activityPoints);
    }

    private void decrementToilet(int toiletPoints) {
        toilet = lowerValueLimiter(toilet, toiletPoints);
    }
    // <-- DECREMENTERS


    // VALUE_LIMITERS -->
    private int upperValueLimiter(int valueField, int pointToAdd) {
        int testValue = valueField += pointToAdd;
        return (testValue > MAXIMUM_VALUE) ? MAXIMUM_VALUE : testValue;
    }

    private int lowerValueLimiter(int valueField, int pointToRemove) {
        int testValue = valueField - pointToRemove;
        return (testValue < MINIMUM_VALUE) ? MINIMUM_VALUE : testValue;
    }
    // <-- VALUE_LIMITERS


    //Depends on activity and naturesCall
    private void happinessCalculator() {


        decrementHappiness(5);

        //Unhappiness under the last hour of the 4 hrs lifecycle
        if (toilet >= 75) {
            decrementHappiness(20);
        }

        if (activity > 75) {

            incrementHappiness(20);

        } else if (activity < 25) {

            decrementHappiness(25);
        }


    }


    public void fourHourCycle() {


        incrementToilet(50);

        decrementActivity(20);

        happinessCalculator();

    }

    public void activityCalculator(int kmWalked){

        int toiletLimit = 200;
        int activityLimit = 500;
        int walkBonus = 10;

        if(kmWalked >= toiletLimit){

            setToilet(0);

        }

        if(kmWalked >= 500){

            int modulu = kmWalked % activityLimit;

            int clean = kmWalked - modulu;

            int result = clean / activityLimit;

            incrementActivity(result * walkBonus);


        }

        incrementHappiness(5);
        happinessCalculator();

    }

}
